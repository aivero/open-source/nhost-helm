# Default values for nhost.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

# nameOverride -- String to partially override the deployment name (will maintain the release name)
nameOverride: ""
# -- String to fully override the deployment name
fullnameOverride: ""

# commonLabels -- Labels to append to all resources
commonLabels: {}
  # foo: bar

imagePullSecrets: []

## Global settings

global:
  logLevel: INFO
  unauthorizedRole: public

  # -- HTTP auth for NHost dashboard and Hasura console. Disabled when password is empty.
  dashboard:
    user: admin
    password: ""

  # -- Shared ingress settings
  ingress:
    hostname: "nhost.local"
    enableTls: true
    # Some services require a specific ingress class to handle e.g. stripping. We include both traefik and nginx. 
    className: traefik
    annotations:
      cert-manager.io/issuer: ca-issuer

  secrets:
    jwtKey: 5152fa850c02dc222631cca898ed1485821a70912a6e3649c49076912daa3b62182ba013315915d64f40cddfbb8b58eb5bd11ba225336a6af45bbae07ca873f3
    # This is the admin secret for Hasura. It is used to access the Hasura console and API.
    admin: nhost-admin-secret

auth:
  # -- Hasura-auth can be disabled when using an external auth prodiver
  enabled: true
  # -- Auth log level
  logLevel: info
  # -- Auth hostname to override the ingress.hostname
  hostname: ""

  # -- Extra env vars
  env:
    AUTH_EMAIL_PASSWORDLESS_ENABLED: 'true'

  # -- Ignored when using Mailhog (mailhog.enabled == true)
  smtp:
    host: smtp.nhost.io
    port: 25
    user: user
    password: changeme
    sender: no-reply@nhost.io

  image:
    repository: nhost/hasura-auth
    pullPolicy: IfNotPresent
    tag: "0.21.0"

  replicaCount: 1
  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 5
    targetCPUUtilizationPercentage: 80
    targetMemoryUtilizationPercentage: 80

  resources:
    limits:
      cpu: 500m
      memory: 512Mi
    requests:
      cpu: 100m
      memory: 256Mi

# -- NHost dashboard
dashboard:
  # -- 
  enabled: true
  # -- Dashboard hostname to override the ingress.hostname
  hostname: ""
  # -- Dashboard env mode
  env: prod

  image:
    repository: nhost/dashboard
    pullPolicy: IfNotPresent
    tag: "0.17.14"

  replicaCount: 1
  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 5
    targetCPUUtilizationPercentage: 80
    targetMemoryUtilizationPercentage: 80

  resources:
    limits:
      cpu: 500m
      memory: 512Mi
    requests:
      cpu: 100m
      memory: 256Mi

# -- Serverless functions
functions:
  # -- Enable serverless functions
  enabled: true
  persistence:
    # -- Persistent volume size
    size: 1Gi

  image:
    repository: nhost/functions
    pullPolicy: IfNotPresent
    tag: "0.1.8"

  replicaCount: 1
  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 5
    targetCPUUtilizationPercentage: 80
    targetMemoryUtilizationPercentage: 80

  resources:
    limits:
      cpu: 500m
      memory: 512Mi
    requests:
      cpu: 100m
      memory: 256Mi

storage:
  # -- Ignored when using Minio
  s3:
    access_key: ""
    secret_key: ""
    endpoint: ""
    bucket: ""

  image:
    repository: nhost/hasura-storage
    pullPolicy: IfNotPresent
    tag: "0.3.5"

  replicaCount: 1
  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 5
    targetCPUUtilizationPercentage: 80
    targetMemoryUtilizationPercentage: 80

  resources:
    limits:
      cpu: 500m
      memory: 512Mi
    requests:
      cpu: 100m
      memory: 256Mi

# -- Main API
graphql:
  enableConsole: true
  logLevel: info
  unauthorizedRole: public

  # -- API and console can have different hostnames. These override the ingress.hostname
  hostnames:
    api: ""
    console: ""

  # -- You can enable persistance before migrations in order to copy the needed files
  # `kubectl cp` can be used for that. In that case you could copy the migrations and metadata manually into the volume. 
  # However, you would also have to disable the initContainer in order to prevent the migrations from running twice.
  persistence:
    enabled: true
    size: 1Gi

  # -- persistence is required for this to work
  migrations:
    enabled: true 
    # It only makes sense to set it true in Aivero, because the empty schema is useless for us

  image:
    repository: hasura/graphql-engine
    pullPolicy: IfNotPresent
    tag: v2.28.0-ce.cli-migrations-v3

  replicaCount: 1
  autoscaling:
    enabled: false
    minReplicas: 1
    maxReplicas: 5
    targetCPUUtilizationPercentage: 80
    targetMemoryUtilizationPercentage: 80

  resources:
    limits:
      cpu: 500m
      memory: 512Mi
    requests:
      cpu: 100m
      memory: 256Mi


##
## 3rd party services used for development
##

mailhog:
  enabled: true

  ingress:
    enabled: true
    ingressClassName: traefik
    hosts:
      - host: mailhog.local
        paths:
          - path: "/"
            pathType: Prefix

minio:
  enabled: true
  persistence:
    size: 1Gi

  defaultBuckets: nhost

  auth:
    rootUser: admin
    rootPassword: youshouldchangethis
    forceNewKeys: true 

##
## PostgreSQL Database
##

postgresql:
  enabled: true

  auth:
    enablePostgresUser: true
    postgresPassword: changethisadminpassword
    username: nhost
    password: changemeaswell
    database: nhost

  image:
    debug: true

  volumePermissions:
    enabled: true

  primary:
    persistence:
      size: 1Gi
    initdb:
      scripts:
        0001-create-schema.sql: |
          CREATE SCHEMA IF NOT EXISTS auth;
          CREATE SCHEMA IF NOT EXISTS storage;
          CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
          CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;
          CREATE OR REPLACE FUNCTION public.set_current_timestamp_updated_at() RETURNS trigger LANGUAGE plpgsql AS $$
          declare _new record;
          begin _new := new;
          _new."updated_at" = now();
          return _new;
          end;
          $$;

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # Annotations to add to the service account
  annotations: {}
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podAnnotations: {}

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

nodeSelector: {}

tolerations: []

affinity: {}
